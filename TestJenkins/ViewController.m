//
//  ViewController.m
//  TestJenkins
//
//  Created by xing on 2020/4/25.
//  Copyright © 2020 xing. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.frame = CGRectMake(100, 100, 100, 40);
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
    self.view.backgroundColor = [UIColor yellowColor];
    NSString *text = @"QA";
#ifdef PP
    text = @"PP";
#endif
#ifdef PD
    text = @"PD";
#endif
    label.text = text;
    
    // Do any additional setup after loading the view.
}


@end
