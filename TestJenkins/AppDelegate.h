//
//  AppDelegate.h
//  TestJenkins
//
//  Created by xing on 2020/4/25.
//  Copyright © 2020 xing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;
@end

